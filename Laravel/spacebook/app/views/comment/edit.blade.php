@extends('layouts.master')

@section('title')
    Lab 10
@stop

@section('column2')

    {{ Form::model($comment, ['method' => 'PATCH', 'route' => ['comment.update', $comment -> id]]) }}    
        <h1>Update post</h1>
    	{{ Form::hidden('user_id', Auth::id()) }}
    	{{ Form::hidden('post_id') }}
        {{  Form::label('message', 'message')  }}
        {{  Form::text('message')  }}
        {{  $errors->first('message')  }}
        {{  Form::submit('Update Post', ['class' => 'btn btn-default'])  }}
    {{  Form::close()  }}
    {{  Form::model($comment, ['method' => 'DELETE', 'route' => ['comment.destroy', $comment->id]])  }}     
        {{  Form::submit('Delete', ['class' => 'btn btn-default'])  }} 
    {{  Form::close()  }}

@stop 
