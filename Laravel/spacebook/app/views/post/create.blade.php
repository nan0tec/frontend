@extends('layouts.master')

@section('title')
    Lab 10
@stop

@section('column2')
  {{ Form::open(['action' => 'PostController@store']) }}
    {{ Form::hidden('user_id', Auth::id()) }}
    {{ Form::label('title', 'title') }}
    {{ Form::text('title') }}
    {{$errors->first('title')}}
    <br>
    {{ Form::label('text', 'text') }}
    {{ Form::text('text') }}
    {{$errors->first('text')}}
    <br>
    {{ Form::label('privacy', 'privacy') }}
    {{ Form::text('privacy') }}
    {{$errors->first('privacy')}}
    <br>
    {{ Form::submit('Create Post', ['class' => 'btn btn-default']) }}
  {{ Form::close() }}
@stop
