@extends('layouts.master')

@section('title')
    Lab 10
@stop

@section('column2')
<h1>Post:</h1>
  <p>User: {{{ $post -> user_id }}}</p>
  <p>Title: {{{ $post -> title }}}</p>
  <p>Text: {{{ $post -> text }}}</p>
  {{ link_to_route('post.edit', 'Edit Post', $post->id) }}
  {{ Form::model($post, ['method' => 'DELETE', 'route' => ['post.destroy', $post['id']]]) }}     
    {{ Form::submit('Delete', ['class' => 'btn btn-default']) }} 
  {{ Form::close() }}
    
  {{ Form::open(['action' => 'CommentController@store']) }}
    {{ Form::hidden('user_id', Auth::id()) }}
    {{ Form::hidden('post_id', $post->id) }}
    {{ Form::label('message', 'message') }}
    {{ Form::text('message') }}
    {{ Form::submit('Create Comment', ['class' => 'btn btn-default']) }}
  {{ Form::close() }}
<br>
@foreach($comments as $comment)
  <p>User: {{{ $comment -> user_id }}}</p>
  <p>Message: {{{ $comment -> message }}}</p>
  {{ Form::model($comment, ['method' => 'DELETE', 'route' => ['comment.destroy', $comment['id']]]) }}     
    {{ Form::submit('Delete', ['class' => 'btn btn-default']) }} 
  {{ Form::close() }}
  {{ link_to_route('comment.edit', 'Edit comment', $comment->id) }}
@endforeach

@stop
