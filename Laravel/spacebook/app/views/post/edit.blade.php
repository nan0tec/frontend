@extends('layouts.master')

@section('title')
    Lab 10
@stop

@section('column2')

    {{ Form::model($post, ['method' => 'PATCH', 'route' => ['post.update', $post -> id]]) }}    
        <h1>Update post</h1>
    	{{ Form::hidden('user_id', Auth::id()) }}
        {{  Form::label('title', 'title')  }}
        {{  Form::text('title')  }}
        {{  $errors->first('title')  }}
        <br>{{ Form::label('text', 'text')  }}
        {{ Form::text('text')  }}
        {{ $errors->first('text')  }}
        <br>
	{{  Form::label('privacy', 'privacy')  }}
        {{  Form::text('privacy')  }}
        {{  $errors->first('privacy')  }}
        <br>
        {{  Form::submit('Update Post', ['class' => 'btn btn-default'])  }}
    {{  Form::close()  }}
    {{  Form::model($post, ['method' => 'DELETE', 'route' => ['post.destroy', $post->id]])  }}     
        {{  Form::submit('Delete', ['class' => 'btn btn-default'])  }} 
    {{  Form::close()  }}

@stop 
