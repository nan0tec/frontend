<!DOCTYPE html>
<!-- master.blade.php -->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Spacebook the social network for astronomers">
    <meta name="author" content="Christopher Mackinga">
    <link rel="icon" href="/2503/lab2/images/favicon.ico">

    <title>Spacebook</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="styles/styles.css " rel="stylesheet">

    <!-- Bootstrap core JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="/js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container-fluid">
      <!-- Static navbar -->
	<nav>
          {{  link_to_route('user.feed', 'Home')  }}
          @if (Auth::check())
            {{ Auth::user()->email }} {{  link_to_route('user.logout', "Sign Out") }}
	    {{ link_to_route('user.show', 'View Profile', Auth::id())  }}
    	    {{ link_to_route('user.edit', 'Edit Profile') }}
          @else
            {{ Form::open(['url' => 'user/login']) }} 
              login
              {{ Form::label('email', 'email') }}
              {{ Form::text('email') }}
              {{ Form::label('price', 'Password') }}
              {{ Form::password('password') }}      
              {{ Form::submit('Sign in', ['class' => 'btn btn-default']) }}
            {{ Form::close() }}
            {{ link_to_route('user.create', 'Create an Account') }}
            {{ Session::pull('login_error') }}
          @endif
        <br><br>
    </nav>

      <div class='row'>
        <div class='col-sm-3 col-md-3 hidden-xs '>
          @yield("column1")
        </div>
        <div class='col-xs-12 col-sm-9 col-md-6'>
          @yield('column2')
        </div>
        <div class='col-md-3 hidden-sm hidden-xs'>
          @yield('column3')
        </div>
        <div class='row'>
          <div class="panel panel-default">
          </div>
        </div>
      </div>
    </div> <!-- /container -->
  </body>
</html>
