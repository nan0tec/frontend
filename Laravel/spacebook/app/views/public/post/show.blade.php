@extends('layouts.master')

@section('title')
    Lab 10
@stop

@section('column2')
<h1>Post:</h1>
    <p>User: {{{ $post -> user_id }}}</p>
    <p>Title: {{{ $post -> title }}}</p>
    <p>Text: {{{ $post -> text }}}</p>
@foreach($comments as $comment)
    <p>User: {{{ $comment -> user_id }}}</p>
    <p>Message: {{{ $comment -> message }}}</p>
@endforeach
@stop
