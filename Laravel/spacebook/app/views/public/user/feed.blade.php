@extends('layouts.master')

@section('title')
    Lab 10
@stop

@section('column2')
<a href="{{{ url('user') }}}">View Users</a>
<h1>Public Posts:</h1>
@foreach($authposts as $post)
    <p>User: {{{ $post -> user_id }}}</p>
    <p>Title: {{{ $post -> title }}}</p>
    <p>Text: {{{ $post -> text }}}</p>
    <p>Privacy: {{{ $post -> privacy }}}</p>
    {{ link_to_route('post.show', 'View Post', $post -> id)  }}
    <br>
@endforeach
@stop
