@extends('layouts.master')

@section('title')
    Lab 10
@stop

@section('column2')
<h1>All Friends:</h1>
@foreach($users as $user)
    <p>Email: {{{ $user->email }}}</p>
    {{ link_to_route('user.show', 'View Profile', $user['id'])  }}
    <br>
@endforeach
@stop
