@extends('layouts.master')

@section('title')
    Lab 10
@stop

@section('column2')
<a href="{{{ url('user') }}}">Add Friends</a>
  <h1>Make A Post:</h1>
  {{ Form::open(['action' => 'PostController@store']) }}
    {{ Form::hidden('user_id', Auth::id()) }}
    {{ Form::label('title', 'title') }}
    {{ Form::text('title') }}
    {{$errors->first('title')}}
    <br>
  {{ Form::label('text', 'text') }}
    {{ Form::text('text') }}
    {{$errors->first('text')}}
    <br>
  {{ Form::label('privacy', 'privacy') }}
    {{ Form::text('privacy') }}
    {{$errors->first('privacy')}}
    <br>
    {{ Form::submit('Create Post', ['class' => 'btn btn-default']) }}
  {{ Form::close() }}
<h1>All Posts:</h1>
@foreach($authposts as $post)
    <p>User: {{{ $post -> user_id }}}</p>
    <p>Title: {{{ $post -> title }}}</p>
    <p>Text: {{{ $post -> text }}}</p>
    <p>Privacy: {{{ $post -> privacy }}}</p>
    {{ link_to_route('post.show', 'View post', $post -> id)  }}
    <br>
@endforeach
@stop
