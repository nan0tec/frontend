@extends('layouts.master')

@section('title')
    Lab 10
@stop

@section('column2')

    {{ Form::model($user, ['method' => 'PATCH', 'route' => ['user.update', $user -> id]]) }}    
        {{ Form::label('email', 'Email') }}
        {{ Form::text('email') }}
        {{$errors->first('email')}}
        <br>
	{{ Form::label('name', 'Name') }}
        {{ Form::text('name') }}
        {{$errors->first('name')}}
        <br>
	{{ Form::label('dob', 'Dob') }}
        {{ Form::text('dob') }}
        {{$errors->first('dob')}}
        <br>
        {{ Form::label('price', 'Password') }}
        {{ Form::password('password') }}      
        {{$errors->first('password')}}
        <br>
        {{ Form::submit('Update Product', ['class' => 'btn btn-default']) }}
    {{ Form::close() }}

    {{ Form::model($user, ['method' => 'DELETE', 'route' => ['user.destroy', $user->id]]) }}     
        {{ Form::submit('Delete', ['class' => 'btn btn-default']) }} 
    {{ Form::close() }}

@stop 
