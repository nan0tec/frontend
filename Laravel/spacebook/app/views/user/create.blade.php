@extends('layouts.master')

@section('title')
    Lab 10
@stop

@section('column2')

    {{ Form::open(['action' => 'UserController@store']) }}
        <h1>Create New User:</h1>
        {{ Form::label('email', 'Email') }}
        {{ Form::text('email') }}
        {{$errors->first('email')}}
        <br>
	{{ Form::label('name', 'Name') }}
        {{ Form::text('name') }}
        {{$errors->first('name')}}
        <br>
	{{ Form::label('dob', 'Dob') }}
        {{ Form::text('dob') }}
        {{$errors->first('dob')}}
        <br>
        {{ Form::label('price', 'Password') }}
        {{ Form::password('password') }}      
        {{$errors->first('password')}}
        <br>
        {{ Form::submit('Create User', ['class' => 'btn btn-default']) }}
    {{ Form::close() }}

@stop
