@extends('layouts.master')

@section('title')
    Profile
@stop

@section('column2')
        <p>Name: {{{ $user->name }}}</p>
        <p>Email: {{{ $user->email }}}</p>
        <p>Date of Birth: {{{ $user->dob }}}</p>
    {{ link_to_route('user.addFriend', 'Add Friend', $user->id) }}
    <br>
    {{  link_to_route('user.index', 'Back to All Users')  }}  
    {{  link_to_route('user.friends', 'Show Friends', $user['id'])  }} 
@foreach($authposts as $post)
    <p>User: {{{ $post -> user_id }}}</p>
    <p>Title: {{{ $post -> title }}}</p>
    <p>Text: {{{ $post -> text }}}</p>
    <p>Privacy: {{{ $post -> privacy }}}</p>
    {{ link_to_route('post.show', 'View post', $post -> id)  }}
    <br>
@endforeach

@stop
