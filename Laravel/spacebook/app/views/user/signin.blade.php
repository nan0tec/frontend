{{--This is the layout for the login page--}}

@extends('layouts.master')

@section('column2')
<div class="panel panel-default">
    <div class ="panel-body">
    <div class="account-wall">
      <h1 class="text-center login-title">Sign in to Spacebook</h1>
      <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt="">
      <form class="form-signin">
      <input type="text" class="form-control" placeholder="Email" required autofocus>
      <input type="password" class="form-control" placeholder="Password" required>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      <label class="checkbox pull-left">
        <input type="checkbox" value="remember-me">Remember me
      </label>
      <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span>
      </form>
      <a href="#" class="text-center new-account">Create an account </a>
    </div>
  </div>
</div>
@stop
