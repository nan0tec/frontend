@extends('layouts.master')

@section('title')
    Lab 10
@stop

@section('column2')

<h1>All Users:</h1>
@foreach($users as $user)
    <p>Email: {{{ $user -> email }}}</p>
    {{ link_to_route('user.show', 'View User', $user -> id)  }}
@endforeach

@stop
