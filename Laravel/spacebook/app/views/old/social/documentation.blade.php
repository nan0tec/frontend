
<ol>
<p>Functional requirements</p>
<li>Implemented navbar on all pages.</li>
<li>Homepage displays a form to add posts.</li>
<li>Homepage displays the posts made with the form.</li>
<li>Posts are displayed in a descending order.</li>
<li>Each post contains an icon.</li>
<li>Each post has a title, a message and a name.</li>
<li>Each post has an edit, delete, an add comment and a view comment button.</li>
<li>Each post lists the number of comments</li>
<li>Clicking the edit button opens a new page where you can edit the post. Clicking cancel on the edit page takes user back to mainpage.</li>
<li>Clicking view comments displays the comments on the same page, while clicking add comment takes the user to a new page for adding comments, which also displays all comments on the post.</li>
<li>The new comments page has a form to add comments with.</li>
<li>The add comments form only lets the user add name and message.</li>
<li>Comments can be deleted on both the new comment page and on the homepage when the view comments button is clicked.</li>
<li>Deleting comments, the user remains on the same page, but the comment has been deleted.</li>
<li>Assignment has been developed using laravel, sqlite and bootstrap.</li>
<p>Non-Functional Requirements</p>
<li>Documentation (checklist for what has been done) is present on the website as separate webpage(s)</li>
<li>ER Diagram on documentation page</li>
<p>Additional Implementations</p>
<li>Began implementing profile system. Different profiles can be accessed by navigating to /0 and /1 at the end of the url. </li>
<li>Began implementing recent friend activity, which shows all posts not attached to the current profile (profile 0 shows recent activity from profile 1 and vice versa)</li>
<li>Began implementing a trending posts column, which would show the most commented/liked posts on the website.</li>
<li>Added the options for a login/logout dropdown menu in the navbar.</li>
<img class="img-responsive center-block" src="{{{ url('images') }}}/erd.png" alt="Entity Relationship Diagram">
</ol>