{{--This is the layout for the main page--}}

@extends('layouts.master')

@section('column1')
  @include('components.displayProfile')
  @include('components.otherPosts')
@stop
@section('column2')
  @include('components.forms.newPostForm')
  @include('components.myPosts')
@stop

@section('column3')
    <div class="panel panel-default">
        <div class="panel-heading text-center">
            <a href="{{ url('/trending') }}">Currently Trending</a>
        </div>
        <div class="panel-body">
        </div>        
    </div>
@stop