{{--This is the for the post page used to view/comment on posts--}}

@extends('layouts.master')

@section('column1')
    
@stop

@section('column2')
    <div class="panel panel-default">
        @include('components.displayPost')
    </div>
    <div class="panel panel-default">
        <div class="panel-heading text-center">
            Comments
        </div>
        <div class="panel-body">
             @include('components.comments')
        </div>
        <div class="panel-footer">
            @include('components.forms.newCommentForm')
        </div>
    </div>
@stop

@section('column3')
   
@stop