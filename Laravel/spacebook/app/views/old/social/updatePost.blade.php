{{--This is the layout for the update post page--}}

@extends('layouts.master')

@section('column1')
    
@stop

@section('column2')
    <div class="panel panel-default">
        @include('components.displayPost')
        <div class="panel-footer">
            @include('components.forms.updatePostForm') 
        </div>
    </div>
@stop

@section('column3')
   
@stop