<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Spacebook the social network for astronomers">
    <meta name="author" content="Christopher Mackinga">
    <link rel="icon" href="/2503/lab2/images/favicon.ico">

    <title>Spacebook</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="styles/styles.css " rel="stylesheet">

    <!-- Bootstrap core JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="/js/bootstrap.min.js"></script>

  </head>

  <body background="/2503ict/lab2/images/starBG.jpg">
    <div class="container-fluid">

      <!-- Static navbar -->
      
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">Spacebook</a>
          </div><!--/.nav-header -->
          
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="active left"><a href="{{ url('photos') }}">Your Photographs</a></li>
              <li><a href="{{ url('/documentation') }}">Documentation</a></li>
              <li class="visible-xs-block visible-sm-block"><a href="{{ url('trending') }}">Currently Trending</a></li>
              <li class="visible-xs-block"><a href="{{ url('profile') }}">User Information</a></li>
              <li class="visible-xs-block"><a href="{{ url('friends') }}">Recent Friends Photos</a></li>
              @if (Auth::check())
                
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Logged in as {{ Auth::user()->username }}<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="{{ url('profile') }}">Account Settings</a></li>
                  <li><a href="{{ url('profile') }}">Edit Profile</a></li>
                  <li><a href="{{ url('profile') }}">Inbox</a></li>
                  <li><a href="{{ url('profile') }}">Notifications</a></li>
                  <li role="separator" class="divider"></li>
                  <li>{{  link_to_route('user.logout', "(Sign Out)") }}</li>
                </ul>
              </li>
              @else

              @endif
            </ul>
          </div><!--/.nav-collapse -->

        </div><!--/.container-fluid -->
      </nav>

    <!-- Main Body -->
    <div class='row'>
      <div class='col-sm-3 col-md-3 hidden-xs '>
        @yield("column1")
      </div>
      <div class='col-xs-12 col-sm-9 col-md-6'>
        @yield('column2')
      </div>
      <div class='col-md-3 hidden-sm hidden-xs'>
        @yield('column3')
      </div>
      <div class='row'>
        <div class="panel panel-default">
        </div>
      </div>
    </div> <!-- /container -->
  </body>
</html>