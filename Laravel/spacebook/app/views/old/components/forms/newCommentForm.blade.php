{{--Displays the form for adding a new comment--}}

<form method="post" action="{{{ url('add_comment_action') }}}">
    <input type="hidden" name="postID" value="{{{ $post->id }}}">
    <input type="hidden" name="userID" value="0">
    <input type="hidden" name="time" value="{{{ Carbon\Carbon::now()->toDateTimeString() }}}">
    <table>
        <tr><td>Comment: </td> <td><textarea name="comment"></textarea></td></tr>
        <tr><td>Name: </td> <td><textarea name="name"></textarea></td></tr>
        <tr><td colspan=2><button type="submit" class="btn btn-default" value="Add Comment">Add Comment</button></td></tr>
        <tr><td colspan=2> @include('components.buttons.cancelBtn') </td></tr>
    </table>
</form>