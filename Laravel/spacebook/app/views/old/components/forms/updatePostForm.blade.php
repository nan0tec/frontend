{{--Displays the form for updating an existing post--}}

<form method="post" action="{{{ url('update_post_action') }}}">
    <input type="hidden" name="id" value="{{{ $post->id }}}">
    <input type="hidden" name="userID" value="{{{ $post->userID }}}">
    <table>
        <tr><td>Icon:</td><td> <select name="icon">
            <option value="sol.jpg">sol</option>
            <option value="mercury.jpy">mercury</option>
            <option value="venus.jpg">venus</option>
            <option value="earth.jpg">earth</option>
            <option value="mars.jpg">mars</option>
            <option value="luna.jpg">luna</option>
            <option value="jupiter.jpg">jupiter</option>
            <option value="saturn.jpg">saturn</option>
            <option value="uranus.jpg">uranus</option>
            <option value="neptune.jpg">neptune</option>
            <option value="pluto.jpg">pluto</option>
            <option value="aurora.gif">aurora</option>
            <option value="bang.gif">bang</option>
            <option value="horizon.gif">horizon</option>
            <option value="orbits.gif">orbits</option>
            <option value="shootingstar.gif">shootingstar</option>
        </select></td></tr>
        <tr><td>Title:</td> <td><input type="text" name="title" value="{{{ $post->title }}}"></td></tr>
        <tr><td>Time:</td> <td><input type="text" name="time" value="{{{ $post->time }}}"></td></tr>
        <tr><td>Description:</td> <td><textarea name="description">{{{ $post->description }}}</textarea></td></tr>
        <tr><td colspan=2><button type="submit" class="btn btn-default" value="Update Post">Update</button></td></tr>
        <tr><td colspan=2> @include('components.buttons.cancelBtn') </td></tr>
    </table>
</form>