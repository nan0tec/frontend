{{--Displays the form for adding a new post--}}

<div class="panel panel-default">
    <div class="panel-body">
        <form method="post" action="add_post_action">
            <input type="hidden" name="userID" value="{{{ $profile->id }}}">
            <input type="hidden" name="time" value="{{{ Carbon\Carbon::now()->toDateTimeString() }}}">
            <table>
                <tr><td>Icon:</td><td> <select name="icon">
                    <option value="sol.jpg">sol</option>
                    <option value="mercury.jpy">mercury</option>
                    <option value="venus.jpg">venus</option>
                    <option value="earth.jpg">earth</option>
                    <option value="luna.jpg">luna</option>
                    <option value="mars.jpg">mars</option>
                    <option value="jupiter.jpg">jupiter</option>
                    <option value="saturn.jpg">saturn</option>
                    <option value="uranus.jpg">uranus</option>
                    <option value="neptune.jpg">neptune</option>
                    <option value="pluto.jpg">pluto</option>
                    <option value="aurora.gif">aurora</option>
                    <option value="bang.gif">bang</option>
                    <option value="horizon.gif">horizon</option>
                    <option value="orbits.gif">orbits</option>
                    <option value="shootingstar.gif">shootingstar</option>
                </select></td></tr>
                <tr><td>Title:</td> <td><input type="text" name="title"></td></tr>
                <tr><td>Description:</td> <td><textarea name="description"></textarea></td></tr>
                <tr><td colspan=2><button type="submit" class="btn btn-default" value="Add Post">Add Post</button></td></tr>
            </table>
        </form>
    </div>
</div> 