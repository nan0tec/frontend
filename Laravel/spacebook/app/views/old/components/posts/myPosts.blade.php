{{--Displays posts for this profile--}}


<div class="panel panel-default">
    <div class="panel-body">
        @foreach($myposts as $post)
            <?php
                $thesecomments = array();
                foreach($comments as $comment) {
                    if ($comment->postID == $post->id) {
                        $thesecomments[] = $comment;
                    }
                }
            ?>
            <div class="panel panel-default">
                @include('components.displayPost')
                <div class="panel-footer">
                    <div class="btn-group" role="group" aria-label="...">
                        @include('components.buttons.updatePostBtn')
                        @include('components.buttons.deletePostBtn')
                        @include('components.buttons.addCommentBtn')
                        <button type="button" class="btn btn-default" data-toggle="collapse" data-target="#comments{{{ $post->id }}}">Comments <span class="badge">{{{ count($thesecomments) }}}</span></button>
                    </div>
                    <div class="collapse" id="comments{{{ $post->id }}}">
                        
                            @include('components.comments')
                        
                    </div>
                </div>
            </div>
        @endforeach
        {{--<button type="button" class="btn btn-primary btn-lg btn-block">Load More</button>--}}
    </div>
</div>