{{--Displays posts for all other profiles--}}

<div class="panel panel-default">
    <div class="panel-heading text-center">
      Recent Friends Photos
    </div>
    <div class="panel-body">
        @if(!empty($otherposts))
            @foreach($otherposts as $post)
                 <?php
                    $thesecomments = array();
                    foreach($comments as $comment) {
                        if ($comment->postID == $post->id) {
                            $thesecomments[] = $comment;
                        }
                    }
                ?>
                <div class="panel panel-default">
                    @include('components.displayPost')
                    <div class="panel-footer">
                        @include('components.buttons.addCommentBtn')
                    </div>
                </div>
            @endforeach 
        @endif
    </div>
</div>