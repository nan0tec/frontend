{{--Displays a single post--}}

<div class="panel-heading text-center">
    {{{ $post -> title }}}
</div>
<div class="panel-body">
    <img class="img-responsive center-block" src="{{{ url('images') }}}/{{{ $post -> icon }}}" alt="Invalid Image">
    <p>Post ID: {{{ $post -> id }}}</p>
    <p>Time: {{{ $post -> time }}}</p>
    <p>Username: {{{ $post -> userID }}}</p>
    <p>Description: {{{ $post -> description }}}</p>
</div>