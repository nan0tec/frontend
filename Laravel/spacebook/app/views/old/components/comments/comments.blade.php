{{--Displays several comments and their delete buttons--}}

@if(!empty($thesecomments))
    @foreach($thesecomments as $comment)
        <div class="panel panel-default">
            @include('components.displayComment')
            <div class="panel-footer">
                @include('components.buttons.deleteCommentBtn')
            </div>
        </div>
    @endforeach
@endif