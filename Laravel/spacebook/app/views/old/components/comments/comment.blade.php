{{--Displays a single comment--}}

<div class="panel-heading text-center">
  On the: {{{ $comment -> time }}}, {{{ $comment -> name }}} Commented:
</div>
<div class="panel-body">
  <p>Comment: {{{ $comment -> comment }}}</p>
</div>