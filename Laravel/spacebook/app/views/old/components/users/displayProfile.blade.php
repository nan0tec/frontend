{{--Displays a single profile--}}

<div class="panel panel-default">
    <div class="panel-heading text-center">
      {{{ $profile -> name }}}
    </div>
    <div class="panel-body text-center">
        <img class="img-responsive center-block" src="images/{{{ $profile -> icon }}}" alt="Invalid Image">
        <p>Email: {{{ $profile -> email }}}</p>
        <p>Join Date: {{{ $profile -> joinDate }}}</p>
        <p>Date of Birth: {{{ $profile -> dob }}}</p>
        <p>Location: {{{ $profile -> location }}}</p>
    </div>
</div>