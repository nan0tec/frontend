<?php

class CommentController extends \BaseController {


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {
		$comment = new Comment;
		$input = Input::all();
		$validator = Validator::make($input, Comment::$rules);
		if ($validator->passes()) {
			$comment->user_id = $input['user_id'];
			$comment->post_id = $input['post_id'];
			$comment->message = $input['message'];
			$comment->save();
  	 	} 
  		return Redirect::back();
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if (!Auth::check()) return Redirect::to('/');
		$comment = Comment::find($id);
		return View::make('comment.edit', compact('comment'));
	}


	public function update($id)
	{
		$comment = Comment::find($id);
		if (Auth::id() == $comment->user_id) {
			$input = Input::all();
			$validator = Validator::make($input, Comment::$rules);
			if ($validator->passes()) {
				$comment->user_id = $input['user_id'];
				$comment->post_id = $input['post_id'];
				$comment->message = $input['message'];
				$comment->save();
  	 		}
		} 
  		return Redirect::route('post.show', $comment->post_id);

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$comment = Comment::find($id);
		if (Auth::id() != $comment->user_id) return Redirect::back();
		$comment -> delete();
		return Redirect::back();
	}


}
