<?php

class UserController extends \BaseController {

	/**
	 * Lists all profiles
	 *
	 * @return Response
	 */
	 

	public function index() {
		$users = User::all();
		return View::make('user.index', compact('users'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		return View::make('user.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {
		$user = new User;
		$input = Input::all();
		$password = $input['password'];
		$encrypted = Hash::make($password);
		$validator = Validator::make($input, User::$rules);
		if ($validator->passes()) {
			$user->email = $input['email'];
			$user->name = $input['name'];
			$user->dob = $input['dob'];
			$user->password = $encrypted;
			$user->save();
  			return Redirect::action('user.show', array($user->id));
  	 	} else {
			return Redirect::action('user.create')->withErrors($validator);
		}
	}	


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$user = User::find($id);
		$authposts = Post::where('user_id', '=', $id)->where('privacy', '=', 0)->get();
		$friends = Friend::all();
		if (Auth::id() == $id) {
			$authposts = $authposts->merge(Post::where('user_id', '=', $id)->where('privacy', '=', 2)->get());
		}
		if (Auth::check()) {
			$authposts = $authposts->merge(Post::where('user_id', '=', $id)->where('privacy', '=', 1)->get());
			foreach ($friends as $friend) {
				if (($friend->user_id == $user->email and $friend->friend_id == Auth::user()->email) or ($friend->friend_id == $user->email and $friend->user_id == Auth::user()->email)) {
					return View::make('user.showDelete', compact('user'), compact('authposts')); 
				}
			}
			return View::make('user.showAdd', compact('user'), compact('authposts')); 
		}
		return View::make('public.user.show', compact('user'), compact('authposts'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @return Response
	 */
	public function edit() {
		if (!Auth::check()) return Redirect::to('/');
		$user = Auth::user();
		return View::make('user.edit', compact('user')); 
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)	{
		if (Auth::id() !=  $id) return Redirect::route('public.user.feed');
		$user = User::find($id);
		$input = Input::all();
		$password = $input['password'];
		$encrypted = Hash::make($password);
		$validator = Validator::make($input, User::$rules);
		if ($validator->passes()) {
			$user->username = $input['username'];
			$user->password = $encrypted;
			$user->save();
  			return Redirect::action('user.show', array($user->id));
  	 	} else {
			return Redirect::action('user.edit', array($user->id))->withErrors($validator);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {	
		$user = User::find($id);
		if (Auth::id() != $user->id) return Redirect::to('/');
		$user -> delete();
		return Redirect::to('user');
	}
	
	public function login() {
		$userdata = ['email' => Input::get('email'), 'password' => Input::get('password')];
		if (Auth::attempt($userdata)) {
			return Redirect::to(URL::previous());
		} else {
			Session::put('login_error', 'Login Failed: Username or password incorrect');
			return Redirect::to(URL::previous())->withInput();
		}
	}
	
	public function friends($id) {
		$user = User::find($id);
		$users = new \Illuminate\Database\Eloquent\Collection;
		foreach (Friend::all() as $friend) {
			if ($friend->user_id == $user->email) {
				$users = $users->merge(User::where('email', '=', $friend->friend_id)->get());
			} else if ($friend->friend_id == $user->email) {
				$users = $users->merge(User::where('email', '=', $friend->user_id)->get());
			}
		}
		return View::make('user.friends', compact('users'));
	}

	public function addFriend($id) {
		$user = User::find($id);
		$friend = new Friend;
		$friend->user_id = Auth::user()->email;
		$friend->friend_id = $user->email;
		$friend->save();
		return Redirect::to(URL::previous());
	}

	public function deleteFriend($id) {
		$user = User::find($id);
		Friend::where('user_id', '=', $user->email)->where('friend_id', '=', Auth::user()->email)->delete();
		Friend::where('user_id', '=', Auth::user()->email)->where('friend_id', '=', $user->email)->delete();
		return Redirect::back();
	}

	public function feed() {
		$posts = Post::all();
		if (!Auth::check()) {
			$authposts = [];
			foreach ($posts as $post) {
				if ($post->privacy == 0) {
					$authposts[] = $post;
				}
			}
			return View::make('public.user.feed', compact('authposts'));
		} else {
			$authposts = [];
			foreach ($posts as $post) {
				if ($post->privacy == 0) {
					$authposts[] = $post;
				} else if ($post->privacy == 1) {
					$authposts[] = $post;
				} else if ($post->privacy == 2 and Auth::id() == $post->user_id) {
					$authposts[] = $post;
				}
			}
			return View::make('user.feed', compact('authposts'));
		}
	}	

	public function logout() {
		Auth::logout();
		return Redirect::to('/');
	}

	public function settings($id) {
		if (!Auth::check()) return Redirect::route('public.user.feed');
		$user = User::find($id);
		return View::make('user.settings', compact('user')); 
	}
}
