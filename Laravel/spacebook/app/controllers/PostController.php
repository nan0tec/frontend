<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Redirect::to('/');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('post.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {
		$post = new Post;
		$input = Input::all();
		$validator = Validator::make($input, Post::$rules);
		if ($validator->passes()) {
			$post->user_id = $input['user_id'];
			$post->title = $input['title'];
			$post->text = $input['text'];
			$post->privacy = $input['privacy'];
			$post->save();
  	 	} 
  		return Redirect::to('/');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$post = Post::find($id);
		$comments = Comment::where('post_id', '=', $id)->get();
		if (!Auth::check()) return View::make('public.post.show', compact('post'), compact('comments'));
		else if ($post->privacy < 2 and Auth::check()) return View::make('post.show', compact('post'), compact('comments'));
		else if ($post->privacy == 2 and Auth::id() ==  $post->user_id) return View::make('post.show', compact('post'), compact('comments'));
		else return Redirect::to('/');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if (!Auth::check()) return Redirect::to('/');
		$post = Post::find($id);
		return View::make('post.edit', compact('post'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$post = Post::find($id);
		if (Auth::id() == $post->user_id) {
			$input = Input::all();
			$validator = Validator::make($input, Post::$rules);
			if ($validator->passes()) {
				$post->user_id = $input['user_id'];
				$post->title = $input['title'];
				$post->text = $input['text'];
				$post->privacy = $input['privacy'];
				$post->save();
  	 		} 
  			return Redirect::route('post.show', compact('post'));
		}
		return Redirect::to('/');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$post = Post::find($id);
		if (Auth::id() != $post->user_id) return Redirect::to('/');
		$post -> delete();
		return Redirect::to('/');
	}


}
