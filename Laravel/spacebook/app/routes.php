<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/

Route::get('user/logout', ['as' => 'user.logout', 'uses' => 'UserController@logout']);
Route::get('/user/{user}/deleteFriend', ['as' => 'user.deleteFriend', 'uses' => 'UserController@deleteFriend']);
Route::get('/user/{user}/addFriend', ['as' => 'user.addFriend', 'uses' => 'UserController@addFriend']);
Route::get('/', ['as' => 'user.feed', 'uses' => 'UserController@feed']);
Route::get('user/{user}/friends', ['as' => 'user.friends', 'uses' => 'UserController@friends']);
Route::post('user/login', ['as' => 'user.login', 'uses' => 'UserController@login']);
Route::resource('post', 'PostController');
Route::resource('user', 'UserController');
Route::resource('comment', 'CommentController');

?>
