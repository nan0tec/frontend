<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table) {
			$table->increments('id');
			$table->string('email')->unique();
			$table->string('password')->index();
			$table->string('remember_token')->nullable();
			$table->string('name');
			$table->date('dob');
            		$table->string('img_filename')->nullable();
            		$table->integer('img_filesize')->nullable();
            		$table->string('img_type')->nullable();
            		$table->timestamp('img_updated_at')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}

}
