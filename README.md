# EthMesh #
## Social Network designed to run on the Ethereum Blockchain

### What is this repository for? ###

This is where I keep the forntend of my dApp

### How do I get set up? ###

Install laravel. Although planning I'm on rebuilding this using React + Redux so this may be obscelete shortly.

### Contribution guidelines ###

I will look at all pull requests but I can't gurantee it will happen in reasonable time as I am often busy
and this is just one of many things I like to do in my free time.

### Who do I talk to? ###

Me, I'm the only person currently working on this, and it's mostly a proof of concept demo project for
testing ideas I have for possible distributed applications.

feel free to look through the code and even use it, just give credit where it is due. Nobody likes plagerism after all.